﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class QuestGame : MonoBehaviour
{
    [SerializeField] string gameTitle;
    [SerializeField] TextMeshProUGUI titleTextComponent;
    [SerializeField] Text storyTextComponent;
    [SerializeField] State startState;

    State currentState;

    // Start is called before the first frame update
    void Start()
    {
        currentState = startState;
        titleTextComponent.text = gameTitle;
    }

    // Update is called once per frame
    void Update()
    {
        ManageStates();
    }
    
    void ManageStates()
    {
        for (int i = 0; i < currentState.GetNextStates().Length; i++)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1 + i))
            {
                currentState = currentState.GetNextStates()[i];
                UpdateStateInfo();
            }
        }
    }

    private void UpdateStateInfo()
    {
        storyTextComponent.text = currentState.GetStoryText();
    }
}
